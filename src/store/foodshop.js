import Vuex from 'vuex';
import Vue from 'vue';
import http from '../util/http';
Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
        categoryList: []
    },
    getters: {
        allcountprice(state) {
            let count = 0;
            let price = 0;
            let shopcarlist = [];
            state.categoryList.forEach(item => {
                if (item.bigcount) {
                    count += item.bigcount * 1;
                    item.spuList.forEach(itm => {
                        if (itm.smallcount) {
                            price += itm.smallcount * itm.currentPrice;
                            itm.tag = item.tag
                            shopcarlist.push(itm)
                        }
                    })
                }
            });
            return { count, price, shopcarlist }
        },
    },
    mutations: {
        getdata(state, data) {
            state.categoryList = data;
        },
        changenum(state, obj) {
            state.categoryList = state.categoryList.map(item => {
                if (item.tag === obj.tag) {
                    obj.type === 'add' ? item.bigcount ? item.bigcount++ : item.bigcount = 1 : item.bigcount--
                        item.spuList.map(itm => {
                            if (itm.spuId === obj.spuId) {
                                obj.type === 'add' ? itm.smallcount ? itm.smallcount++ : itm.smallcount = 1 : itm.smallcount--
                            }
                        })
                }
                return item //forEach不能有返回值
            })
        }
    },
    actions: {
        getdatasync(contextStorte, url) {
            return new Promise(resolve => {
                setTimeout(() => {
                    http.get(url).then(res => {
                        let data = JSON.parse(res.data.data).categoryList
                        contextStorte.commit('getdata', data);
                    })
                }, 1000)
            })
        }
    },
})
export default store