import Vue from 'vue';
import app from './view/app.vue';
import router from './router';
import store from './store/foodshop.js';
console.log(router)
window.eventBus = new Vue();
Vue.prototype.bus = new Vue();
new Vue({
    el: '#app',
    router,
    store,
    components: {
        app
    },
    template: '<app/>'
})