import Router from 'vue-router';
import Vue from 'vue';

Vue.use(Router);
let router = new Router({
    mode: 'hash',
    routes: [{
        path: '/',
        redirect: '/foodshop'
    }, {
        path: '/artical',
        component: () =>
            import ('@/view/article')
    }, {
        path: '/home',
        component: () =>
            import ('@/view/home')
    }, , {
        path: '/foodshop',
        component: () =>
            import ('@/view/foodshop')
    }]
})
export default router;