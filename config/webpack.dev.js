const path = require('path');
const webpack = require('webpack');
const uglify = require('uglifyjs-webpack-plugin');
const htmlPlugin = require('html-webpack-plugin');
const extractTextPlugin = require('extract-text-webpack-plugin'); //注意版本号
const VueLoaderPlugin = require('vue-loader/lib/plugin'); //vue-template-compiler要下载这个包
const fs = require('fs');
const resolve = function(url) {
    return path.join(__dirname, '..', url)
}

//配置成绝对路径
var website = {
        publicPath: "http://localhost:8888/"
            // publicPath:"http://192.168.1.103:8888/"
    }
    //这里的IP和端口，是你本机的ip或者是你devServer配置的IP和端口。
module.exports = {
    mode: 'development',
    //入口文件的配置项
    entry: {
        main: './src/main.js',
        main2: './src/main2.js' //多页面才多入口文件  所以一般不会涉及到
    },
    //出口文件的配置型
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: '[name].js', //名字跟属性一样  更改这里面的代码build命令执行 不是server
        publicPath: website.publicPath //publicPath：主要作用就是处理静态文件路径的。
    },
    //模块：例如解读css,图片如何转换，压缩
    module: {
        rules: [
            //scss loader   node-sass  sass-loader
            {
                test: /\.scss$/,
                use: extractTextPlugin.extract({
                        use: [{
                            loader: "css-loader"
                        }, {
                            loader: "sass-loader"
                        }],
                        // use style-loader in development
                        fallback: "style-loader"
                    })
                    // use: [{
                    //     loader: "style-loader" // creates style nodes from JS strings
                    // }, {
                    //     loader: "css-loader" // translates CSS into CommonJS
                    // }, {
                    //     loader: "sass-loader" // compiles Sass to CSS
                    // }]
            },


            //less loader
            {
                test: /\.less$/,
                use: extractTextPlugin.extract({
                        use: [{
                            loader: "css-loader"
                        }, {
                            loader: "less-loader"
                        }],
                        // use style-loader in development
                        fallback: "style-loader"
                    })
                    // use: [{
                    //         loader: "style-loader" // creates style nodes from JS strings
                    //     },
                    //     {
                    //         loader: "css-loader" // translates CSS into CommonJS
                    //     },
                    //     {
                    //         loader: "less-loader" // compiles Less to CSS
                    //     }
                    // ]
            },
            //css loader
            {
                test: /\.css$/,
                use: extractTextPlugin.extract({ //css分离后这里需要重新配置
                        fallback: "style-loader",
                        use: "css-loader",
                        // use: "style-loader!css-loader!postcss-loader"
                    })
                    // use: [{  css分离后这里需要重新配置，下面就注释了
                    //     loader: 'style-loader'
                    // }, {
                    //     loader: 'css-loader'
                    // }]
            },
            {
                test: /\.(png|jpg|gif|jpeg)/, //是匹配图片文件后缀名称
                use: [{
                    loader: 'url-loader', //是指定使用的loader和loader的配置参数
                    options: {
                        limit: 500, //是把小于500B的文件打成Base64的格式，写入JS
                        outputPath: 'images/', //打包后的图片放到images文件夹下
                    }
                }]
            }, {
                test: /\.(htm|html)$/i,
                use: ['html-withimg-loader']
            }, {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.vue', '.json'], //需要配置
        alias: {
            'vue': 'vue/dist/vue.esm.js',
            '@': resolve('src')
        }
    },
    //插件：用域生产模板和各项功能
    plugins: [
        new uglify(), //js压缩插件
        new htmlPlugin({
            minify: { //是对html文件进行压缩
                removeAttributeQuotes: true //removeAttrubuteQuotes是却掉属性的双引号。
            },
            hash: true, //为了开发中js有缓存效果，所以加入hash，这样可以有效避免缓存JS。
            template: './src/index.html' //是要打包的html模版路径和文件名称

        }),
        new extractTextPlugin('style.css'),
        // 在plugins选项中添加这两个内置插件
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new VueLoaderPlugin()
    ],
    //配置webpack开发服务功能
    devServer: {
        contentBase: path.resolve(__dirname, '../dist'),
        host: 'localhost',
        compress: true,
        port: 8888,
        //在devServer配置中添加该项
        hot: true,
        before(app) {
            const fooddata = fs.readFileSync(resolve('src/mock/data.json')).toString();
            app.get('/getdata', (req, res) => {
                res.send({ code: 1, data: fooddata })
            })
        }
    }
}