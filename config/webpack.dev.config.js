import webpackdev from './webpack.dev.js';
const glob = require('glob');
const PurifyCSSPlugin = require("purifycss-webpack");
module.exports = merge(webpackdev, {
    module: {
        // rules: []
    },
    babel: [{
        test: /\.(jsx|js)$/,
        use: {
            loader: 'babel-loader',
            options: {
                presets: [
                    "es2015", "react"
                ]
            }
        },
        exclude: /node_modules/
    }, {
        test: /\.css$/,
        use: extractTextPlugin.extract({
            fallback: "style-loader",
            use: [{ loader: "css-loader" },
                {
                    loader: "postcss-loader",
                },
            ]
        })
    }],
    // eval-source-map is faster for development
    // devtool: '#eval-source-map',
    plugins: [
        new PurifyCSSPlugin({
            //这里配置了一个paths，主要是需找html模板，purifycss根据这个配置会遍历你的文件，查找哪些css被使用了。
            paths: glob.sync(path.join(__dirname, 'src/*.html')),
        })
    ]
})